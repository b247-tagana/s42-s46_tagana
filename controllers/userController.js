const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Order = require("../models/Order");


const Product = require("../models/Product");

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.authenticate = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return false;
	
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (data) => {
	console.log(data.userId);
  return User.findById(data.userId)
    .then((result) => {
      if (!result) {
        console.log("User not found");
        return null;
      }
      result.password = "";
      return result;
    })
    .catch((error) => {
      console.log("Error finding user:", error);
      return null;
    });
};