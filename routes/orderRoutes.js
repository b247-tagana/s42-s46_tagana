const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");

// Authenticated non-admin user (checkout)
router.post("/checkout", (req, res) => {
	orderController.checkout(req.body).then(
		resultFromController => res.send(resultFromController));
});


module.exports = router;
