const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

// Register email
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Check the email
router.post("/login", (req, res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/authenticate", (req, res) => {

	userController.authenticate(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/userDetails", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData.id);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;